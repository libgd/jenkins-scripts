#!/bin/sh
rm -rf .checkscripts
git clone --depth=1 https://bitbucket.org/libgd/jenkins-scripts.git .checkscripts || exit 1
exec .checkscripts/build-and-check.sh
