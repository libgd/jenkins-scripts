#!/bin/sh

. $(dirname "$0")/functions
cleanup

coverity_succeeded()
{
	# succeed only if all units were compiled successfully
	tail -n2 "$1" \
	| sed -r 's@^[^>]*>\s+@@' \
	| grep -q '\(C/C++ compilation units (100%) are ready for analysis\|cov-build utility completed successfully\)'
}

# parameters
CONFIGURE_OPTIONS=""
COVERITY_INTERMEDIATE_DIR="cov-int"
RESULTS_ARCHIVE=libgd-dns-cov-int.tgz

# bootstrap and analyse
set -xe
autoreconf -fi
./configure $CONFIGURE_OPTIONS
cov-build --dir "$COVERITY_INTERMEDIATE_DIR" make all check
set +xe

# check the result
if ! coverity_succeeded "$COVERITY_INTERMEDIATE_DIR/build-log.txt"; then
	echo "The scan-build failed." >&2
	exit 1
fi

# add SCM data
cov-import-scm --scm git --dir cov-int 2>/dev/null

# create the archive to be uploaded
tar czf "$RESULTS_ARCHIVE" "$COVERITY_INTERMEDIATE_DIR"

echo "Archive $RESULTS_ARCHIVE is ready for upload." >&2
exit 0
