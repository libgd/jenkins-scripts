#!/bin/sh

. $(dirname "$0")/functions

RESULTS_ARCHIVE=libgd-dns-cov-int.tgz
TOKEN="$1"

if [ ! -r "$RESULTS_ARCHIVE" ]; then
	echo "Archive does not exist." >&2
	exit 1
fi

if [ -z "$TOKEN" ]; then
	echo "No submit token provided." >&2
	echo "usage: $0 <token>" >&2
	exit 1
fi

curl \
	--form project=LibGD \
	--form token="$TOKEN" \
	--form email=ondrej.sury@nic.cz \
	--form file=@"$RESULTS_ARCHIVE" \
	--form version="$(build_version)" \
	--form description="$(build_commit) in $(build_branch)" \
	http://scan5.coverity.com/cgi-bin/upload.py
